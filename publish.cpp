//
// Created by shurtado on 3/8/2021.
//
#include <iostream>
#include <string>
#include <mqtt/async_client.h>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

const string TOPIC {"hello"};

const int QOS=1;

int main(int argc, char* argv[])
{
    const string address{"tcp://192.168.1.68:1883"};
    Mat img = imread("particles.jpg", IMREAD_COLOR);
    if(img.empty())
    {
        std::cout << "Could not read the image: "<< std::endl;
        return 1;
    }
    std::vector<uchar> buff;
    auto param = std::vector<int>(2);
    param[0] = IMWRITE_JPEG_QUALITY;
    param[1] = 95;
    imencode(".jpg",img,buff,param);

    std::string s(buff.begin(),buff.end());

    cout << "Initializing for server '" << address << "'..." << endl;
    mqtt::async_client client(address, "");
    try{
        client.connect()->wait();
        mqtt::topic topic(client,TOPIC,QOS);
        mqtt::token_ptr tok;
        tok = topic.publish(s);
        tok->wait();
        client.disconnect()->wait();
        cout<<"SEND"<<endl;
    }catch (const mqtt::exception& exc) {
        cerr << exc << endl;
        return 1;
    }

    return 0;
}
