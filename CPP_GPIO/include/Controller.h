//
// Created by Santiago Hurtado on 30.08.21.
//

#ifndef CAMERACONTROLLER_CONTROLLER_H
#define CAMERACONTROLLER_CONTROLLER_H

#include <drivers/CameraDriver.h>
#include "Event.h"
#include "module/GpioController.h"

using namespace CameraController::module;

namespace CameraController {
    class Controller {
    public:
        Controller();

    private:
        void ListenToMessages();

        std::unique_ptr<GpioController> gpioController;
        std::shared_ptr<EventManager> eventManager;
        std::unique_ptr<Drivers::CameraDriver> cameraReader;
        std::unique_ptr<Drivers::KeyboardDriver> keyboardDriver;

        void quit();
    };
}

#endif //CAMERACONTROLLER_CONTROLLER_H
