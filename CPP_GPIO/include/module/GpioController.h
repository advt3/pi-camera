//
// Created by Santiago Hurtado on 24.09.21.
//

#ifndef CAMERA_CONTROLLER_MODULE_GPIOCONTROLLER_H
#define CAMERA_CONTROLLER_MODULE_GPIOCONTROLLER_H
/**
 *
 * @author Santiago Hurtado
 */
#include <thread>
#include <gpiod.hpp>
#include "Event.h"

namespace CameraController::module {
    class GpioController {
    public:
        GpioController(const std::shared_ptr<EventManager> &eventManager);

        ~GpioController();

        void Run();

        void WaitForEvents();

        void Stop();

    private:
        void Notify(EventMessage message);

        std::atomic<bool> isRunning{false};

        /**
         * Listen to messages from the input devices
         */
        void ListenToMessages();

        /**
         * Change the light status for the camera
         * @param status
         */
        void changeLightStatus(bool status);

        /**
         * Message queue for events
         */
        std::shared_ptr<CameraController::EventManager> eventManager;
        /**
         * Maintains the status if the camera is on or off
         */
        std::atomic<bool> isDeviceOn{false};

        std::thread thread;

        gpiod::chip chip;

        gpiod::line lightLine;

    };
}
#endif //CAMERA_CONTROLLER_MODULE_GPIOCONTROLLER_H
