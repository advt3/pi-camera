//
// Created by Santiago Hurtado on 30.08.21.
//

#ifndef CAMERACONTROLLER_EVENT_H
#define CAMERACONTROLLER_EVENT_H


#include <string>
#include <boost/signals2.hpp>

namespace CameraController {
    enum class Event {
        REQUEST_VIDEO_ON,
        REQUEST_VIDEO_OFF,
        VIDEO_ON,
        VIDEO_OFF,
        QUIT,
        EVENT_DETECTED,
        NONE
    };

    struct EventMessage {
        explicit EventMessage(const std::string cmd) : command(cmd) {
            event = Event::NONE;
        }

        explicit EventMessage(const Event e) : event(e) {
            command = "";
        }

        Event event;
        std::string command;
    };

    struct EventManager {
        boost::signals2::signal<void(EventMessage *)> events;
    };
}

#endif //CAMERACONTROLLER_EVENT_H
