//
// Created by Santiago Hurtado on 18.09.21.
//

#ifndef CAMERACONTROLLER_DRIVEREXCEPTION_H
#define CAMERACONTROLLER_DRIVEREXCEPTION_H

struct DriverException : public std::exception {
    std::string message{};

    explicit DriverException(std::string exception) : message(exception) {

    }

    [[nodiscard]] const char *what() const noexcept {
        return message.c_str();
    }
};

#endif //CAMERACONTROLLER_DRIVEREXCEPTION_H
