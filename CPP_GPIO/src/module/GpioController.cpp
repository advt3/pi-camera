//
// Created by Santiago Hurtado on 24.09.21.
//
#include <boost/program_options.hpp>
#include <iostream>
#include "include/module/GpioController.h"

namespace opt = boost::program_options;
using namespace std::chrono_literals;

namespace CameraController::module {

    void GpioController::Run() {
        isRunning = true;
        ListenToMessages();
        WaitForEvents();
    }

    GpioController::GpioController(const std::shared_ptr<EventManager> &eventManager) {
        this->eventManager = eventManager;
        chip = gpiod::chip("/dev/gpiochip0");
        lightLine = chip.get_line(21);
        lightLine.request({"statusLight",
                           gpiod::line_request::DIRECTION_OUTPUT, 0}, 0);
    }

    void GpioController::ListenToMessages() {
        eventManager->events.connect([this](CameraController::EventMessage *e) {
            //std::cout << static_cast<std::underlying_type<CameraController::Event>::type>(e->event) << std::endl;
            if (e->event == Event::QUIT) {
                return;
            } else if (e->event == Event::VIDEO_ON) {
                changeLightStatus(true);
                isDeviceOn = true;
            } else if (e->event == Event::VIDEO_OFF) {
                changeLightStatus(false);
                isDeviceOn = false;
            }
        });
    }

    void GpioController::WaitForEvents() {
        thread = std::thread([this] {
            auto buttonLine = chip.get_line(15);
            buttonLine.request({"button",
                                gpiod::line_request::EVENT_BOTH_EDGES,
                                gpiod::line_request::FLAG_ACTIVE_LOW}, 1);
            while (isRunning) {
                auto event = buttonLine.event_wait(10s);
                if (event && buttonLine.get_value() == 1) {
                    if (isDeviceOn) {
                        Notify(EventMessage(CameraController::Event::REQUEST_VIDEO_OFF));
                        std::this_thread::sleep_for(1s);
                    } else {
                        Notify(EventMessage(CameraController::Event::REQUEST_VIDEO_ON));
                        std::this_thread::sleep_for(1s);
                    }
                }
            }
        });
    }

    GpioController::~GpioController() {
        changeLightStatus(false);
        if (thread.joinable()) {
            thread.join();
        }
    }

    void GpioController::Stop() {
        this->isRunning = false;
    }

    void GpioController::changeLightStatus(bool status) {
        lightLine.set_value(status);
        isDeviceOn = status;
    }

    void GpioController::Notify(EventMessage message) {
        eventManager->events(&message);
    }

}
