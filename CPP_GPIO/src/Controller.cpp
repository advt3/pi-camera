//
// Created by Santiago Hurtado on 30.08.21.
//
#include <drivers/KeyboardDriver.h>
#include "Controller.h"

namespace CameraController {
    Controller::Controller() {
        eventManager = std::make_shared<EventManager>();
        keyboardDriver = std::make_unique<Drivers::KeyboardDriver>(eventManager);
        cameraReader = std::make_unique<Drivers::CameraDriver>(eventManager);
        gpioController = std::make_unique<GpioController>(eventManager);
        gpioController->Run();
        this->ListenToMessages();
        keyboardDriver->ReadCommands();
    }

    void Controller::ListenToMessages() {
        eventManager->events.connect([this](EventMessage *e) {
            if (e->command == "quit") {
                quit();
                return;
            } else if (e->event != Event::NONE) {
                switch (e->event) {
                    case Event::QUIT:
                        std::cout << "quit" << std::endl;
                        quit();
                        return;
                    case Event::EVENT_DETECTED:
                        break;
                    case Event::NONE:
                        break;
                }
            }
        });
    }

    void Controller::quit() {
        cameraReader->Stop();
        gpioController->Stop();
    }
}