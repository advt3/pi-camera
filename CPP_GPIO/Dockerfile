FROM ubuntu:20.04
ENV DEBIAN_FRONTEND="noninteractive"
ARG PASSWORD=${PASSWORD}

RUN apt-get update && apt-get install -yqq tzdata \
    autoconf autoconf-archive gcc g++ git libgtest-dev build-essential cmake libopencv-dev libssl-dev autoconf doxygen graphviz\
    autoconf-archive libtool libkmod-dev pkg-config ssh g++ gdb clang rsync tar\
    libboost-program-options-dev libboost-tools-dev libboost-log-dev libboost-regex-dev libboost-system-dev\
    libprotobuf-dev protobuf-compiler\
    && apt-get clean

WORKDIR /opt
# Additional Libs
RUN git clone https://github.com/eclipse/paho.mqtt.c.git \
    && cd paho.mqtt.c\
    && cmake -Bbuild -H. -DPAHO_ENABLE_TESTING=OFF -DPAHO_BUILD_STATIC=ON -DPAHO_WITH_SSL=ON -DPAHO_HIGH_PERFORMANCE=ON\
    && cmake --build build/ --target install\
    && ldconfig

RUN git clone https://github.com/eclipse/paho.mqtt.cpp\
    && cd paho.mqtt.cpp\
    && cmake -Bbuild -H. -DPAHO_BUILD_STATIC=ON -DPAHO_BUILD_DOCUMENTATION=FALSE -DPAHO_BUILD_SAMPLES=FALSE\
    && cmake --build build/ --target install\
    && ldconfig

RUN git clone https://git.kernel.org/pub/scm/libs/libgpiod/libgpiod.git \
    && cd libgpiod\
    && ./autogen.sh --enable-tools=yes --enable-bindings-cxx\
    && make\
    && make install && make doc \

RUN ( \
    echo 'LogLevel INFO'; \
    echo 'PermitRootLogin yes'; \
    echo 'PasswordAuthentication yes'; \
    echo 'Subsystem sftp /usr/lib/openssh/sftp-server';) > /etc/ssh/sshd\
  && mkdir /run/sshd

RUN useradd -m shurtado && yes ${PASSWORD} | passwd shurtado

RUN usermod -s /bin/bash shurtado

CMD ["/usr/sbin/sshd", "-D", "-e", "-f", "/etc/ssh/sshd"]