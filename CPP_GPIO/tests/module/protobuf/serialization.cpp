//
// Created by Santiago Hurtado on 03.11.21.
//
#include <iostream>
#include <fstream>
#include "schema/events.pb.h"

int main(){
    std::cout<<"Start"<<std::endl;
    Action action;
    action.set_datetime(20);
    action.set_auth_key("auth£--$£*()`--%$^£");
    action.set_event(Event::REQUEST_VIDEO_ON);
    std::ofstream ofs("action.data", std::ios_base::out | std::ios_base::binary);
    action.SerializeToOstream(&ofs);
    ofs.close();

    Action read;
    std::fstream ifs("action.data", std::ios_base::in | std::ios_base::binary);
    read.ParseFromIstream(&ifs);
    std::cout<<read.auth_key()<<Event_Name(read.event())<<std::endl;

    return 0;
}