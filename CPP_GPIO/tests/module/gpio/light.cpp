#include <chrono>
#include <iostream>
#include <thread>

#include <gpiod.h>
#include <gpiod.hpp>

using namespace std::literals::chrono_literals;

const int lightPin = 21;

/**
 * Turning the light on and off
 * @return
 */
int main() {
    gpiod::chip chip("/dev/gpiochip0");
    auto line = chip.get_line(lightPin);
    line.request({"test", gpiod::line_request::DIRECTION_OUTPUT, 0}, 0);
    auto counter = 0;
    while (counter < 100) {
        line.set_value(1);
        std::cout << "ON" << std::endl;
        std::this_thread::sleep_for(500ms);
        line.set_value(0);
        std::cout << "OFF" << std::endl;
        std::this_thread::sleep_for(500ms);
        counter++;
    }
    line.release();
    chip.reset();
    return 0;
}