#include <thread>
#include <iostream>
#include <atomic>

//
// Created by Santiago Hurtado on 06.10.21.
//
class TestClass {
public:
    TestClass() {
        std::cout << "Create" << std::endl;
    }

    void RunThread() {
        thread = std::thread([this] {
            while (counter.load() < 10) {
                std::cout << "Counter: " << counter << std::endl;
                counter++;
            }
        });
    }

    ~TestClass() {
        std::cout << "Destroy" << std::endl;
        if (thread.joinable()) {
            thread.join();
        }
    }

private:
    std::thread thread;
    std::atomic<int> counter{0};
};

int main() {
    auto test = TestClass();
    test.RunThread();
    return 0;
}
