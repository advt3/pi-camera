#include <chrono>
#include <iostream>
#include <thread>

#include <gpiod.h>
#include <gpiod.hpp>

using namespace std::literals::chrono_literals;

const int buttonPin = 15;
const int lightPin = 21;

/**
 * Turning the light on and off
 * @return
 */
int main() {
    gpiod::chip chip("/dev/gpiochip0");

    auto buttonLine = chip.get_line(buttonPin);
    auto lightLine = chip.get_line(lightPin);

    lightLine.request({"light", gpiod::line_request::DIRECTION_OUTPUT, 0}, 0);
    buttonLine.request({"button", gpiod::line_request::EVENT_BOTH_EDGES, gpiod::line_request::FLAG_ACTIVE_LOW}, 1);

    int count = 0;
    bool lightOn = false;

    while (count < 100) {
        auto event = buttonLine.event_read();
        if (event.source.get_value() == 0) {
            if (lightOn) {
                lightLine.set_value(0);
                lightOn = false;
            } else {
                lightLine.set_value(1);
                lightOn = true;
            }
        }
        count++;

    }
    return 0;
}