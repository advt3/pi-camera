//
// Created by Santiago Hurtado on 07.12.21.
//

#include <gtest/gtest.h>
#include "Event.h"

using namespace CameraController;

TEST(EventSerialization, SerializeMessageNotEmpty) {
    auto event = EventMessage();
    auto default_serialization = Serializer::SerializeEvent(event);
    // std::cout<<default_serialization<<std::endl;
    EXPECT_TRUE(default_serialization.length()>0);
}
TEST(EventSerialization, DeserializeDefaultMessageNotEmpty) {
    std::string value = "{\n"
                       "    \"event\": \"NONE\",\n"
                       "    \"timestamp\": \"0\",\n"
                       "    \"command\": \"\"\n"
                       "}";
    auto deserialized_event = Serializer::DeserializeEvent(value);
    EXPECT_TRUE(deserialized_event.event=="NONE");
}
TEST(EventSerialization, DeserializeNotEmpty) {
    std::string value = "{\"event\": \"EVENT_DETECTED\",\"timestamp\": \"100\",\"command\": \"Command\"}";
    auto deserialized_event = Serializer::DeserializeEvent(value);
    EXPECT_TRUE(deserialized_event.event=="EVENT_DETECTED");
    EXPECT_TRUE(deserialized_event.timestamp==100);
    EXPECT_TRUE(deserialized_event.command=="Command");
}
TEST(EventSerialization, DeserializeEmptyJson) {
    std::string value = "{}";
    auto deserialized_event = Serializer::DeserializeEvent(value);
    EXPECT_TRUE(deserialized_event.event=="NONE");
}
TEST(EventSerialization, DeserializeEmptyString) {
    std::string value = "";
    auto deserialized_event = Serializer::DeserializeEvent(value);
    EXPECT_TRUE(deserialized_event.event=="NONE");
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}