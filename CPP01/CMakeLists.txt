cmake_minimum_required(VERSION 3.15)
project(CameraController)

set(CMAKE_CXX_STANDARD 17)
OPTION(GPIO "BUILD GPIO" OFF)
find_package(Boost COMPONENTS thread log log_setup program_options REQUIRED)
find_package(OpenCV REQUIRED)

find_package(PahoMqttCpp REQUIRED)
find_package(Threads REQUIRED)

include_directories(. include ${BOOST_INCLUDE_DIRS})

add_executable(${PROJECT_NAME}
        main.cpp
        src/Controller.cpp
        src/drivers/KeyboardDriver.cpp
        src/drivers/CameraDriver.cpp
        src/drivers/ButtonDriver.cpp
        src/drivers/LightDriver.cpp
        src/Event.cpp
        src/io/MqttClient.cpp
        include/drivers/Driver.h)

target_link_libraries(${PROJECT_NAME}
        Threads::Threads
        ${OpenCV_LIBS}
        Boost::log
        Boost::log_setup
        Boost::program_options
        PahoMqttCpp::paho-mqttpp3)

if (GPIO)
    #MESSAGE(SEND_ERROR "IO must be built with GPIO=ON.")
    #add_executable(light src/io/light.cpp)
endif ()
unset(GPIO CACHE)

add_subdirectory(tests)
