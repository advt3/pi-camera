//
// Created by Santiago Hurtado on 30.08.21.
//

#include <thread>
#include <chrono>
#include <future>
#include <DriverException.h>
#include "drivers/CameraDriver.h"

namespace CameraController::Drivers {
    void CameraDriver::Run() {
        if(isRunning){
            // if already running leave it running
            return;
        }
        isRunning = true;
        // Open the camera stream
        if (!cap.isOpened()) {
            try {
                cap.open(0);
            } catch (const std::exception &e) {
                std::cerr << e.what() << std::endl;
            }
        }
        if (!cap.isOpened()) {
            throw DriverException("Error opening video stream or file");
        }
        cap.set(cv::CAP_PROP_FPS, 1);
        // Start reading the stream
        cameraThread = std::thread([this] {
            while (isRunning) {
                cv::Mat frame;
                try {
                    cap >> frame;
                } catch (const std::exception &e) {
                    std::cerr << e.what() << std::endl;
                }
                if (!frame.empty()) {
                    //we make a copy to not interfere with the video thread
                    /*auto f = std::async([this, frame] {
                        auto newFrame = std::make_shared<cv::Mat>(frame);
                        auto found = processor->ProcessFrame(newFrame);
                        if (found) {
                            WriteFrame(newFrame);
                        }
                    });*/
                    std::this_thread::sleep_for(std::chrono::milliseconds(200));
                } else {
                    std::cout << "No frame read" << std::endl;
                    break;
                }
                //eventManager->events(new EventMessage("new frame"));
            }
        });
    }

    void CameraDriver::Stop() {
        isRunning = false;
        if(cameraThread.joinable()){
            cameraThread.join();
        }
        cap.release();
    }
}
