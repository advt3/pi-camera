//
// Created by Santiago Hurtado on 30.08.21.
//
#include "drivers/KeyboardDriver.h"

namespace CameraController::Drivers {
    void KeyboardDriver::ReadCommands() {
        isRunning = true;
        keyboardThread = std::thread([this] {
            std::string s, input;
            std::cout << "Write a command to execute" << std::endl;
            while (isRunning && std::getline(std::cin, s, '\n')) {
                input = std::move(s);
                this->Notify(EventMessage(input));
            }
        });
        if (keyboardThread.joinable()) {
            keyboardThread.join();
        }
    }
}
