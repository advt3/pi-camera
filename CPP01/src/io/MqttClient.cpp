//
// Created by Santiago Hurtado on 07.12.21.
//
#include "MqttClient.h"

namespace CameraController::Mqtt {

    const int QOS = 1;
    const int N_RETRY_ATTEMPTS = 5;

    int Client::Subscribe() {
        // A subscriber often wants the server to remember its messages when its
        // disconnected. In that case, it needs a unique ClientID and a
        // non-clean session.

        mqtt::async_client cli(serverAddress, clientId);

        mqtt::connect_options connOpts;
        connOpts.set_clean_session(false);

        // Install the callback(s) before connecting.
        callback cb(cli, connOpts,"hello",clientId,this->eventManager);
        cli.set_callback(cb);

        // Start the connection.
        // When completed, the callback will subscribe to topic.

        try {
            std::cout << "Connecting to the MQTT server..." << std::flush;
            cli.connect(connOpts, nullptr, cb);
        }
        catch (const mqtt::exception &exc) {
            std::cerr << "\nERROR: Unable to connect to MQTT server: '"
                      << serverAddress << "'" << exc << std::endl;
            return 1;
        }

        // Just block till user tells us to quit.

        while (std::tolower(std::cin.get()) != 'q');

        // Disconnect

        try {
            std::cout << "\nDisconnecting from the MQTT server..." << std::flush;
            cli.disconnect()->wait();
            std::cout << "OK" << std::endl;
        }
        catch (const mqtt::exception &exc) {
            std::cerr << exc << std::endl;
            return 1;
        }
        return 0;
    }
    void action_listener::on_failure(const mqtt::token &tok) {
        std::cout << name_ << " failure";
        if (tok.get_message_id() != 0)
            std::cout << " for token: [" << tok.get_message_id() << "]" << std::endl;
        std::cout << std::endl;
    }

    void action_listener::on_success(const mqtt::token &tok) {
        std::cout << name_ << " success";
        if (tok.get_message_id() != 0)
            std::cout << " for token: [" << tok.get_message_id() << "]" << std::endl;
        auto top = tok.get_topics();
        if (top && !top->empty())
            std::cout << "\ttoken topic: '" << (*top)[0] << "', ..." << std::endl;
        std::cout << std::endl;
    }


    void callback::reconnect() {
        std::this_thread::sleep_for(std::chrono::milliseconds(2500));
        try {
            cli_.connect(connOpts_, nullptr, *this);
        }
        catch (const mqtt::exception &exc) {
            std::cerr << "Error: " << exc.what() << std::endl;
            exit(1);
        }
    }

    // Re-connection failure
    void callback::on_failure(const mqtt::token &tok) {
        std::cout << "Connection attempt failed" << std::endl;
        if (++nretry_ > N_RETRY_ATTEMPTS)
            exit(1);
        reconnect();
    }


    void callback::connected(const std::string &cause) {
        std::cout << "\nConnection success" << std::endl;
        std::cout << "\nSubscribing to topic '" << topic << "'\n"
                  << "\tfor client " << clientId
                  << " using QoS" << QOS << "\n"
                  << "\nPress Q<Enter> to quit\n" << std::endl;

        cli_.subscribe(topic, QOS, nullptr, subListener_);
    }


    void callback::connection_lost(const std::string &cause) {
        std::cout << "\nConnection lost" << std::endl;
        if (!cause.empty())
            std::cout << "\tcause: " << cause << std::endl;

        std::cout << "Reconnecting..." << std::endl;
        nretry_ = 0;
        reconnect();
    }

    // Callback for when a message arrives.
    void callback::message_arrived(mqtt::const_message_ptr msg) {
        std::cout << "Message arrived" << std::endl;
        std::cout << "\ttopic: '" << msg->get_topic() << "'" << std::endl;
        std::cout << "\tpayload: '" << msg->to_string() << "'\n" << std::endl;
        auto event = Serializer::DeserializeEvent(msg->to_string() );
        eventManager->events(&event);
        std::cout << "Message send" << std::endl;
    }



}
