//
// Created by Santiago Hurtado on 30.08.21.
//
#include <drivers/KeyboardDriver.h>
#include <future>
#include "Controller.h"
#include "MqttClient.h"

namespace CameraController {
    Controller::Controller() {
        eventManager = std::make_shared<EventManager>();
        keyboardDriver = std::make_unique<Drivers::KeyboardDriver>(eventManager);
        this->ListenToMessages();
        Mqtt::Client("tcp://10.1.1.108:1883", "cpp_test", eventManager).Subscribe();
        keyboardDriver->ReadCommands();
        cameraReader = std::make_unique<Drivers::CameraDriver>(eventManager);
    }

    void Controller::ListenToMessages() {
        eventManager->events.connect([this](EventMessage *e) {
            std::cout << e->command << std::endl;
            if (e->command == "quit") {
                return;
            } else if (e->eventType != Event::NONE) {
                switch (e->eventType) {
                    case Event::QUIT:
                        std::cout << "quit" << std::endl;
                        return;
                    case Event::VIDEO_ON:
                        std::cout << "video on" << std::endl;
                        cameraReader->Run();
                        break;
                    case Event::VIDEO_OFF:
                        cameraReader->Stop();
                        break;
                    case Event::EVENT_DETECTED:
                        break;
                    case Event::NONE:
                        break;
                }
            }else{
                std::cerr<<e->event<<std::endl;
            }
        });
    }
}