//
// Created by Santiago Hurtado on 07.12.21.
//

#ifndef CAMERACONTROLLER_MQTTCLIENT_H
#define CAMERACONTROLLER_MQTTCLIENT_H

#include <string>
#include <utility>
#include "mqtt/async_client.h"
#include "Event.h"
#include "drivers/Driver.h"

namespace CameraController::Mqtt {
    class Client : public Drivers::Driver {
    public:

        Client(std::string ip, std::string id, const std::shared_ptr<EventManager> &eventManager) :
                serverAddress(std::move(ip)),
                clientId(std::move(id)),
                Driver(eventManager) {
        }

        int Subscribe();

    private:
        std::string serverAddress;
        std::string clientId;
    };

    class action_listener : public virtual mqtt::iaction_listener {
    public:
        explicit action_listener(std::string name) : name_(std::move(name)) {}

    private:
        std::string name_;

        void on_failure(const mqtt::token &tok) override;

        void on_success(const mqtt::token &tok) override;
    };

    class callback : public virtual mqtt::callback,
                     public virtual mqtt::iaction_listener {
    private:
        std::shared_ptr<EventManager> eventManager;
        std::string topic;
        std::string clientId;
        // Counter for the number of connection retries
        int nretry_;
        // The MQTT client
        mqtt::async_client &cli_;
        // Options to use if we need to reconnect
        mqtt::connect_options &connOpts_;
        // An action listener to display the result of actions.
        action_listener subListener_;

        // This demonstrates manually reconnecting to the broker by calling
        // connect() again. This is a possibility for an application that keeps
        // a copy of its original connect_options, or if the app wants to
        // reconnect with different options.
        // Another way this can be done manually, if using the same options, is
        // to just call the async_client::reconnect() method.
        void reconnect();

        void on_failure(const mqtt::token &tok) override;

        // (Re)connection success
        // Either this or connected() can be used for callbacks.
        void on_success(const mqtt::token &tok) override {}

        // (Re)connection success
        void connected(const std::string &cause) override;

        // Callback for when the connection is lost.
        // This will initiate the attempt to manually reconnect.
        void connection_lost(const std::string &cause) override;

        // Callback for when a message arrives.
        void message_arrived(mqtt::const_message_ptr msg) override;

        void delivery_complete(mqtt::delivery_token_ptr token) override {}

    public:
        callback(mqtt::async_client &cli,
                 mqtt::connect_options &connOpts,
                 std::string t,
                 std::string c,
                 std::shared_ptr<EventManager> eventManager) :
                eventManager(std::move(eventManager)),
                nretry_(0),
                cli_(cli),
                connOpts_(connOpts),
                subListener_("Subscription"),
                topic(std::move(t)),
                clientId(std::move(c)) {}
    };
}
#endif //CAMERACONTROLLER_MQTTCLIENT_H
