//
// Created by Santiago Hurtado on 30.08.21.
//

#ifndef CAMERACONTROLLER_EVENT_H
#define CAMERACONTROLLER_EVENT_H


#include <string>
#include <chrono>
#include <boost/signals2.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/log/trivial.hpp>

using namespace std::chrono;
using namespace boost::property_tree;

namespace CameraController {
    enum class Event {
        VIDEO_ON,
        VIDEO_OFF,
        QUIT,
        EVENT_DETECTED,
        NONE
    };

    struct EventMessage {
        explicit EventMessage(const std::string cmd) : command(cmd) {
            eventType = Event::NONE;
            timestamp = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
        }
        EventMessage(){
            eventType = Event::NONE;
            event = "NONE";
            timestamp = 0;
            command = "";
        }
        Event eventType;
        std::string event;

        std::string command;
        long timestamp;

    };

    struct EventManager {
        boost::signals2::signal<void(EventMessage *)> events;
    };
    struct Serializer{
        std::string static SerializeEvent(EventMessage message){
            std::stringstream ss;
            ptree response;
            response.put("event",message.event);
            response.put("timestamp",message.timestamp);
            response.put("command",message.command);
            boost::property_tree::write_json(ss,response);
            return ss.str();
        }
        EventMessage static DeserializeEvent(const std::string &message){
            ptree pt;
            std::stringstream ss;
            ss.str(message);
            auto msg = EventMessage();
            try{
                boost::property_tree::read_json(ss, pt);
                msg.command = pt.get<std::string>("command");
                msg.timestamp = pt.get<long>("timestamp");
                msg.event = pt.get<std::string>("event");
            }catch (ptree_bad_path const &exception) {
                BOOST_LOG_TRIVIAL(warning) << "Bad Path Exception caught when parsing message "<< exception.what() << std::endl;
                return msg;
            }catch (ptree_bad_data const &exception) {
                BOOST_LOG_TRIVIAL(warning) << "Bad Data Exception caught when parsing message "<< exception.what() << std::endl;
                return msg;
            }catch (std::exception const &exception) {
                BOOST_LOG_TRIVIAL(warning)<< "Exception caught when parsing message"<< exception.what()<< std::endl;;
                return msg;
            }
            return msg;
        }
    };
}

#endif //CAMERACONTROLLER_EVENT_H
