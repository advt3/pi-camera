//
// Created by Santiago Hurtado on 30.08.21.
//

#ifndef CAMERACONTROLLER_CONTROLLER_H
#define CAMERACONTROLLER_CONTROLLER_H

#include <drivers/CameraDriver.h>
#include <drivers/LightDriver.h>
#include "Event.h"

namespace CameraController {
    class Controller {
    public:
        Controller();

    private:
        void ListenToMessages();

        std::shared_ptr<EventManager> eventManager;
        std::unique_ptr<Drivers::CameraDriver> cameraReader;
        std::unique_ptr<Drivers::LightDriver> cameraStatusLight;
        std::unique_ptr<Drivers::LightDriver> notificationStatusLight;
        std::unique_ptr<Drivers::KeyboardDriver> keyboardDriver;
    };
}

#endif //CAMERACONTROLLER_CONTROLLER_H
