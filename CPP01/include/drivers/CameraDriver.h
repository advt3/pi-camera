//
// Created by Santiago Hurtado on 30.08.21.
//
#ifndef CAMERACONTROLLER_CAMERADRIVER_H
#define CAMERACONTROLLER_CAMERADRIVER_H

#include "Driver.h"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/background_segm.hpp>

using namespace cv;
namespace CameraController::Drivers {
   /**
    * Handles the camera status and allow to process each frame
    * @author Santiago Hurtado
    */
    class CameraDriver : public Driver {
    public:
        explicit CameraDriver(const std::shared_ptr<EventManager>& eventManager) : Driver(eventManager) {
            eventManager->events.connect([this](EventMessage *e) {
                if (e->eventType == Event::VIDEO_ON) {
                    std::cout << "start video" << std::endl;
                } else if (e->eventType == Event::VIDEO_OFF) {
                    std::cout << "stop video" << std::endl;
                }
            });
        }

        /**
         * Start reading the camera stream
         */
        void Run();

        /**
         * Stop reading the camera stream and close
         */
        void Stop();

    private:
        cv::VideoCapture cap;
        std::thread cameraThread;
    };
}
#endif //CAMERACONTROLLER_CAMERADRIVER_H
