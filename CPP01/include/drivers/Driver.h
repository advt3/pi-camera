//
// Created by Santiago Hurtado on 30.08.21.
//

#ifndef CAMERACONTROLLER_DRIVER_H
#define CAMERACONTROLLER_DRIVER_H

#include "include/Event.h"
#include <memory>
#include <iostream>

namespace CameraController::Drivers {
    /**
     * Abstract class for all the drivers to be subscribed to the event manager
     * @author Santiago Hurtado
     */
    class Driver {
    protected:
        std::shared_ptr<EventManager> eventManager;
        std::atomic<bool> isRunning{};
    public:
        explicit Driver(const std::shared_ptr<EventManager>& eventManager) {
            this->eventManager = eventManager;
            eventManager->events.connect([this](EventMessage *e) {
                if (e->command == "quit") {
                    isRunning = false;
                }
            });
        }

        virtual void Notify(EventMessage message) {
            eventManager->events(&message);
        }
    };
}
#endif //CAMERACONTROLLER_DRIVER_H
