//
// Created by Santiago Hurtado on 30.08.21.
//

#ifndef CAMERACONTROLLER_KEYBOARDDRIVER_H
#define CAMERACONTROLLER_KEYBOARDDRIVER_H


#include "Driver.h"
#include <iostream>
#include <thread>

namespace CameraController::Drivers {
    /**
     * Allows to read the keyboard and create commands for the drivers
     * @author Santiago Hurtado
     */
    class KeyboardDriver : public Driver {
    public:
        explicit KeyboardDriver(const std::shared_ptr<EventManager>& eventManager) : Driver(eventManager) {

        }

        void ReadCommands();

        ~KeyboardDriver() {
            std::cout << "exiting" << std::endl;
        }

    private:
        std::thread keyboardThread;
    };
}


#endif //CAMERACONTROLLER_KEYBOARDDRIVER_H
