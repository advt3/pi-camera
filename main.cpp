#include <iostream>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <string>
#include <mqtt/async_client.h>

using namespace cv;
using namespace std;

int main() {
    const string address{"tcp://192.168.1.68:1883"};
    const string TOPIC{"hello"};
    const int QOS = 1;
    auto param = std::vector<int>(2);
    param[0] = IMWRITE_JPEG_QUALITY;
    param[1] = 95;
    std::vector<uchar> buff;
    VideoCapture cap;
    cap.open(0);
    if (!cap.isOpened()) {
        std::cout << "Error opening video stream or file" << std::endl;
        return -1;
    }
    cout << "Initializing for server '" << address << "'..." << endl;
    mqtt::async_client client(address, "");
    auto count = 0;
    try {
        client.connect()->wait();
        mqtt::topic topic(client, TOPIC, QOS);
        while (true) {
            Mat frame, flipped;
            cap >> frame;
            if (!frame.empty()) {
                flip(frame,flipped, -1);
                // Send every 3 frames
                if (count % 3 == 0) {
                    mqtt::token_ptr tok;
                    imencode(".jpg", flipped, buff, param);
                    std::string s(buff.begin(), buff.end());
                    tok = topic.publish(s);
                    tok->wait();
                }
            } else {
                cout << "No frame read" << endl;
                break;
            }
            char c = (char) waitKey(25);
            if (c == 27)
                break;
            if(count>100){
                std::cout<<"100 frames send"<<std::endl;
                count=0;
            }
            ++count;
        }
    } catch (const mqtt::exception &exc) {
        cerr << exc << endl;
        return 1;
    }
    cap.release();
    client.disconnect()->wait();
    return 0;
}